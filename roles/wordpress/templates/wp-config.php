<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', '{{ mysql_db_name }}');

/** MySQL database username */
define('DB_USER', '{{ mysql_user }}');

/** MySQL database password */
define('DB_PASSWORD', '{{ mysql_password }}');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '])Q&dKc#sj:uH$fF+70?D@?dKHD_;gk.5lAA&wnXxDje5GxIfgCDBo3]of/=|_ww');
define('SECURE_AUTH_KEY',  'XA{VAt]hl@{/U/#+f7|+Q;(<@{YdJP U%d7Y *Ge[_(yAw%98Bf@xlGD)_&:+S,F');
define('LOGGED_IN_KEY',    'qv)r-^eFpSOy(1ss*;Wj<Z4z7<_`Cguqy3%s6M*q &l#e)?^9fM4;n6sloj:i4x]');
define('NONCE_KEY',        '5aoP}OY0/7,;qc_,wbNvbru4W%CN(Ga0.tPaWj*o(dE50rJh+hQ:)haT~VIWSS#)');
define('AUTH_SALT',        '5+e<#3X8YIwceS!I1QaS$D`gNd4P_-v^vfUnQB[Kqa`]WuonY%/}BmN]+RHNhEK4');
define('SECURE_AUTH_SALT', 'fL4%=DEX03(&;o?lQ4qy^ ;G!F8Ny}#/6YP0wWmH?[21J]-RWP(?GwmzR%)y^@GX');
define('LOGGED_IN_SALT',   '.HM%Xh&$2ekek<?~|bv?XO]zqo%|-f^nE%;IjG}==4^;Dh.O^a DL,uQ,ySip2Mh');
define('NONCE_SALT',       '7=1/)zWj8?SV<ST~i%nI+EV+PcG9QzRmZfmJO5xy4Fv:m]QUX;1Fl?gj6Nbi?sO~');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
        define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
