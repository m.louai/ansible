# Ansible

Consigne d'un Yanis => pas de Ansible galaxy tant qu'on ne maîtrise pas les playbooks.

Structure du Readme :

O / Ressources

I / Arborescence avec draw.io

II / Installation d'Ansible

III / Exécution de différents script Yaml

IV / Les quatres fichiers de rôles

	A) mysql
	B) munin
	C) nginx
	D) wordpress

V / Les optimisations (vars ...)

VI / Pièges à newbees

## O / Sites de ressources

Tout est dans l'url

https://linuxfr.org/users/skhaen/journaux/deploiement-et-automatisation-avec-ansible-partie-1

Script nginx selon la distribution linux

https://steemit.com/ansible/@rnason/installing-nginx-using-ansible

https://docs.ansible.com/ansible/latest/user_guide/playbooks.html

## I / Arborescence des fichiers

https://gitlab.com/m.louai/ansible/blob/master/images/Arborescence_des_fichiers(1).png

## II / Installation d'Ansible sur nos machines locales

CMD
~~~~
$ sudo apt-get update
$ sudo apt-get install software-properties-common
$ sudo apt-add-repository ppa:ansible/ansible
$ sudo apt-get update
$ sudo apt-get install ansible
~~~~

**Création du fichier de configuration hosts**

Dans le fichier hosts sur la machine locale:
~~~~
$ cd /etc/ansible
$ sudo vim hosts 
Ajouter en tête de fichier => [Adresse Ip de la machine hébergeant le site wordpress]
~~~~

CMD machine de contrôle
~~~~
ansible all -m ping
ansible all -m setup
~~~~

**Fichiers par défault**

~~~~
cd /etc/ansible/
~~~~

- ansible.cfg : la configuration d'ansible, celle par défaut nous convient parfaitement bien pour le moment, mais ça peut valoir le coup d'y jeter un coup d'oeil pour voir ce que l'on peut faire.
- hosts : c'est dans ce fichier que nous allons indiquer nos serveurs, étape obligatoire pour la suite.
- roles : ce dossier nous servira surtout par la suite quand on commencera à avoir plusieurs rôles à nos playbooks.

**Script pour installer paquets & dépendances => le playbook**

~~~~
[lien vers wordpress_install.yml]
~~~~

## III / Exécution de différents script Yaml

Possibilité de faire l'appel de plusieurs scripts via un seul... =>  wordpress_install.yml 

**Exécuter un script avec Ansible Ach so!**

~~~~
ansible-playbook --private-key [pathto/private_key] [pathto/script.yml]
~~~~

**Erreur avec la mise en place du fichier de config www**

~~~~
sudo cp /etc/ansible/*
~~~~

## IV / Les quatres fichiers de rôles

**mysql**

Main.yml => Mise en place de la base de données avec ses users

**wordpress**

wp-config.php => MySQL settings ; Secret keys ; Database table prefix ; ABSPATH

Main.yml => Installation de wordpress sur l'adresse ip de la machine virtuelle

**nginx**

Main.yml => Sert à copier le fichier de configuration sur le virtual host

wordpress.conf => Ecoute sur le port 80

**munin**

TO DO => 

## V / Les optimisations 

**group_vars**

Nous avons adopté une arborescence de dossiers centrée autour des rôles.
Il allait donc de soi d'utiliser la méthode de création d'un dossier group_vars pour y insérer les variables de tout le projet.

[url vers documentation de Ansible sur les variables]

## VI / Pièges à newbees

1 / Ansible ne cherche pas Python dans le bon repository, création lien symbolique vers dossier Python dans machine virtuelle :

CLI machine distante
~~~~
sudo ln -s /usr/bin/python3 /usr/bin/python
~~~~

2 / -i hosts oublié

Pour lancer la commande ansible-playbook, il faut prendre garde de bien pointer vers le fichiers "host" configuré dans le projet, et pas celui par défaut qui existe dans le /etc/ansible.
Pour cela, il nous faut ajouter les paramètres "-i hosts".

~~~~
ansible-playbook --private-key [pathto/private_key] [pathto/script.yml] -i hosts 
~~~~

3 / Erreur 404 lors de la tentative d'accès au site

Il faut configurer le fichier PHP www.conf de Wordpress en y ajoutant :

~~~~
;listen.allowed_clients = 127.0.0.1
~~~~



